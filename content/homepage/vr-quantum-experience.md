---
title: 'VR Quantum Experience'
weight: 49
header_menu: true
template: 'light-grey'
---

A VR Quantum Experience é uma experiência imersiva e interativa em Realidade Virtual, projetada para facilitar a compreensão de conceitos fundamentais da mecânica quântica.

Durante a demonstração, os participantes mergulham em uma representação das funções de onda de Schrödinger aplicadas ao átomo de hidrogênio. Em um ambiente virtual com 6 graus de liberdade, será possível explorar diferentes configurações dos orbitais atômicos, movendo-se livremente ao redor do átomo, ampliando ou reduzindo sua escala e rotacionando-o. 

Essa abordagem dinâmica oferece uma perspectiva única e acessível para a visualização de fenômenos quânticos complexos!

[*Ver gravação*](https://www.youtube.com/live/yWPuctfjIao?si=-g47_hfwd1iFr0E5)