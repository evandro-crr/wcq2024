---
title: 'Programação'
weight: 20
header_menu: true
template: 'odd'
---

<div class="schedule-container">
    <table style="margin-bottom: 20px;">
        <tr>
            <td style="border: none;"></td>
            <td class="wspace">&nbsp;</td>
            <th>TERÇA-FEIRA (29/10)</th>
            <th>QUARTA-FEIRA (30/10)</th>
            <th>QUINTA-FEIRA (31/10)</th>
        </tr>
        <tr class="table-space">
            <td style="width: 11%;">&nbsp;</td>
            <td style="width: 3%;">&nbsp;</td>
            <td style="width: 28.6%">&nbsp;</td>
            <td style="width: 28.7%">&nbsp;</td>
            <td style="width: 28.6%">&nbsp;</td>
        </tr>
        <tr>
            <th>9h-10h</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#introdução-à-programação-quântica-com-ket">
                    Introdução à programação quântica com Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#analisando-abordagens-clássicas-e-quânticas-para-otimização-combinatória-testando-speedups-quadráticos-em-algoritmos-heurísticos">
                    Analisando Abordagens Clássicas e Quânticas para Otimização Combinatória: Testando Speedups Quadráticos em Algoritmos Heurísticos (Pedro Contino)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#explorando-problemas-da-física-com-aprendizado-de-máquina-e-algoritmos-variacionais-quânticos">
                    Explorando Problemas da Física com Aprendizado de Máquina e Algoritmos Variacionais Quânticos (Felipe Fanchini)
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>10h-10h30</th>
            <td class="wspace">&nbsp;</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
        </tr>
        <tr>
            <th>10h30-11h30</th>
            <td class="wspace">&nbsp;</td>
            <td rowspan="2">
                <a class="schedule-a" href="#introdução-à-programação-quântica-com-ket">
                    Introdução à programação quântica com Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#tecnologias-emergentes-em-computação-quântica-com-átomos-neutros">
                    Tecnologias Emergentes em Computação Quântica com Átomos Neutros (Manuel Alejandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td rowspan="2">
                <a class="schedule-a" href="#exposição-de-pôsteres">
                    Exposição de Pôsteres
                </a>
                / 
                <a class="schedule-a" href="#vr-quantum-experience">
                    VR Quantum Experience
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>11h30-12h</th>
            <td class="wspace">&nbsp;</td>
            <!-- <td></td> -->
            <td>
                <a class="schedule-a" href="#vr-quantum-experience">
                    VR Quantum Experience (DEMO)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <!-- <td></td> -->
        </tr>
        <tr class="table-space">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>14h-15h30</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#introdução-à-programação-quântica-com-ket">
                    Introdução à programação quântica com Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#aprendizado-de-máquina-quântica">
                    Aprendizado de máquina quântica (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/jNtEqG7oarvuY8EW7">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    INE106
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#aprendizado-de-máquina-quântica">
                    Aprendizado de máquina quântica (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
        </tr>
        <tr>
            <th>15h30-16h</th>
            <td class="wspace">&nbsp;</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
        </tr>
        <tr>
            <th>16h-17h</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#aprendizagem-no-universo-quântico">
                    Aprendizagem no Universo Quântico (Robert Huang)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#aprendizado-de-máquina-quântica">
                    Aprendizado de máquina quântica (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/jNtEqG7oarvuY8EW7">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    INE106
                </a>
            </td>
            <td rowspan="2">
                <a class="schedule-a" href="#lightning-talks">
                    Lightning Talks
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>17h-18h</th>
            <td class="wspace">&nbsp;</td>
            <td>
            </td>
            <td>
                <a class="schedule-a" href="#podcast-quântico">
                    Podcast Quântico
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <!-- <td></td> -->            
        </tr>
    </table>
</div>

<style>
    div.schedule-container {
        width: 100%;
        font-family: "Montserrat";
        font-size: 1.5rem;
        margin-top: 50px;
    }

    div.schedule-container table a.schedule-a {
        text-decoration: none;
        color: #E5D6FF;
        font-family: "Montserrat";
        font-weight: 400;
    }
    div.schedule-container a.schedule-a:hover {
        color: white;
    }

    div.schedule-container table td,
    div.schedule-container table th {
        text-align: center;
        border: 2px solid #A56EFF;
    }

    div.schedule-container table td {
        position: relative;
        color: #E5D6FF;
        line-height: 2rem;
        padding: 20px 10px;
    }

    div.schedule-container table th {
        background-color: #3F235F;
    }

    div.schedule-container tr.table-space td,
    div.schedule-container tr.table-space th {
        border: none;
        line-height: 25px;
        height: 25px;
        padding: 0;
    }

    div.schedule-container td.wspace {
        border: none;
    }

    div.schedule-container tr:nth-child(n+3) th {
        line-height: 6rem;
    }

    div.schedule-container a.location {
        text-decoration: none;
        font-size: 1.2rem;
        opacity: 0.4;
        cursor: pointer;
        position: absolute;
        right: 0;
        bottom: 0;
        padding: 5px 5px;
    }

</style>


As atividades serão realizadas nos seguintes locais:

- [Auditório João Ernesto Escosteguy Castro](https://maps.app.goo.gl/bye1MVZTx8WnAJiUA) (Auditório do [EPS](https://maps.app.goo.gl/Fu4kG4S2bP7Vemkq5))
- [Laboratório LIICT6](https://maps.app.goo.gl/cE8pS84qQbsF1u3N9) no 3º andar do [Bloco B do CTC](https://maps.app.goo.gl/9oEVkChJnQENDGgu6)
- Laboratório INE106, localizado no andar térreo do [INE](https://maps.app.goo.gl/9dQLzi9Mqf8GCgFZ6)
- Auditório do [Departamento de Arquitetura e Urbanismo](https://maps.app.goo.gl/1ZfqMN7JU88SkNXA9)
