---
title: 'Speakers'
weight: 50
header_menu: true 
template: 'odd'
---

--------------------------------------

## Evandro C. R. da Rosa
*Quantuloop (Brazil)*

![Evandro C. R. da Rosa](../fotos/evandro.jpg)

Evandro is the co-founder of the quantum computing startup Quantuloop, and he is a PhD and MSc candidate in Computer Science at the Federal University of Santa Catarina (UFSC). His research focuses on quantum computer programming and simulation. He is the creator of the open-source quantum programming platform Ket, leading its development within the UFSC Quantum Computing Group.

--------------------------------------

## Felipe Fanchini
*São Paulo State University (Brazil)*

![Felipe Fanchini](../fotos/felipe.jpg)

Felipe is a professor and researcher at the São Paulo State University (Unesp) with experience in quantum information and computation, as well as machine learning. He is a member of the coordination team for the FAPESP program in quantum technologies. His research is related to open quantum systems, quantum information protection strategies, and the analysis of quantum systems using machine learning techniques. In recent years, he has focused on quantum machine learning and quantum optimization, highlighting the confluence of his main areas of expertise.

--------------------------------------

## Hsin-Yuan (Robert) Huang
*Caltech (USA)*

![Hsin-Yuan (Robert) Huang](../fotos/robert.jpg)

Hsin-Yuan Huang is a visiting scientist at MIT and a research scientist at Google Quantum AI. He earned his Ph.D. in 2023 under the guidance of John Preskill and Thomas Vidick. Also known as Robert, he will join Caltech as an Assistant Professor of Theoretical Physics in 2025. His research focuses on quantum information theory, quantum many-body physics, and learning theory to advance our understanding of quantum phenomena and accelerate scientific discovery. He aims to develop quantum machines capable of exploring new frontiers in quantum mechanics and beyond.

--------------------------------------

## Lucas Friedrich
*Federal University of Santa Maria (Brazil)*

![Lucas Friedrich](../fotos/lucas.jpg)

Lucas Friedrich holds a Bachelor's degree in Physics from the Federal University of Santa Maria, where he is currently pursuing his Ph.D. in Physics. His research is focused on the emerging field of quantum machine learning, exploring how quantum computers can offer significant advantages in creating more robust and efficient models compared to classical methods. Additionally, he is involved in developing new optimization methods and the practical application of variational quantum algorithms.

--------------------------------------

## Manuel A. Lefrán Torres
*University of São Paulo (Brazil)*

![Manuel A. Lefrán Torres](../fotos/manuel.jpg)

Manuel Alejandro Lefrán Torres holds a bachelor's degree in Physics from the Central University "Marta Abreu" of Las Villas (2014), a master's degree in Applied Mathematics from the same institution (2017), and a Ph.D. in Physics from the University of São Paulo (2024). He is currently a postdoctoral researcher at the University of São Paulo, working on the project "Molecular Beam in a Single Quantum State". He has experience in the field of Physics, with an emphasis on Atomic and Molecular Physics, and is interested in research related to cold atoms and molecules, quantum sensors, and quantum computing.

--------------------------------------

## Pedro C. da Silva Costa
*University of Technology of Sydney (Australia)*

![Pedro C. da Silva Costa](../fotos/pedro.jpg)

Pedro C. S. Costa is an Assistant Researcher at the University of Technology Sydney (UTS) and an Honorary Researcher at Macquarie University. Additionally, he consults for two American startups, one of which is BosonQ Psi. He completed his master's degree in Theoretical Physics at Unesp and his doctorate at the Brazilian Center for Research in Physics under the supervision of Fernando de Melo. During his Ph.D., he began specializing in quantum algorithms, with his first significant result being a quantum algorithm for wave equation simulation, developed with Stephen Jordan during his time as a visiting researcher at the University of Maryland in the United States. He conducted his postdoctoral research at Macquarie University in Sydney, Australia, with Prof. Dominic Berry's group. Subsequently, he worked for a year and a half for the Australian government, aiming to apply quantum solutions to the transportation network. He has developed the most efficient quantum algorithms for both linear and nonlinear differential equations and the most efficient algorithm possible for solving systems of linear equations.

--------------------------------------

## Rômulo Pinho
*Dell (Brazil)*

![Rômulo Pinho](../fotos/romulo.jpg)

Rômulo Pinho is a researcher and Distinguished Engineer at Dell Technologies, primarily working in the fields of AI and Quantum Computing, helping the company explore and define its role at the intersection of these two areas. He holds a PhD in Physics (medical image processing) from the University of Antwerp, Belgium, and has 25 years of experience in R&D across industry and academia, in sectors such as IT, O&G, healthcare, and television. Rômulo joined Dell Technologies (formerly EMC) in 2014 as an AI researcher. Since then, he has led research in areas such as data compression, scientific workflow and workload optimization, pattern and anomaly detection, computer vision, NLP, and more recently, at the intersections of quantum computing and generative AI. He has published over 20 scientific papers in journals and conferences and has more than 100 patents filed.

--------------------------------------

## Tiago de Souza Farias
*Federal University of São Carlos (Brazil)*

![Tiago de Souza Farias](../fotos/tiago.jpg)

Tiago Farias holds a Ph.D. in Physics from the Federal University of Santa Maria and is currently pursuing postdoctoral research at the Federal University of São Carlos, focusing on quantum computing and machine learning. With experience in both classical and quantum algorithms, his research interests include quantum computing, statistical physics, and optimization. Tiago is dedicated to exploring the frontiers of quantum technology applied to machine learning, aiming to make significant contributions to the field.

--------------------------------------

## Waldemir Cambiucci
*Microsoft (Brazil)*

![Waldemir Cambiucci](../fotos/waldemir.jpg)

Waldemir is the Director of Innovation and Emerging Technologies at Microsoft Brazil, supporting discussions on digital transformation and innovation with corporate clients. He has been involved in projects and discussions on IoT, Industrial IoT, Artificial Intelligence, Machine Learning, Responsible AI, Cloud Solutions, and Quantum Solutions Architecture. With over 25 years of experience in IT, he has participated in major technology forums in Brazil and abroad. Waldemir holds a degree in Computer Engineering and a Master's in Electrical Engineering from the Polytechnic School of the University of São Paulo (EPUSP). He is currently a PhD candidate at the University of São Paulo, researching quantum algorithms and distributed quantum computing. He is a member of the IEEE Quantum Community, a Qiskit Developer Associate Certified, and a Microsoft Azure Quantum Ambassador in Brazil.
