---
title: 'Schedule'
weight: 20
header_menu: true
template: 'odd'
---

<div class="schedule-container">
    <table style="margin-bottom: 20px;">
        <tr>
            <td style="border: none;"></td>
            <td class="wspace">&nbsp;</td>
            <th>TUESDAY (29/10)</th>
            <th>WEDNESDAY (30/10)</th>
            <th>THURSDAY (31/10)</th>
        </tr>
        <tr class="table-space">
            <td style="width: 11%;">&nbsp;</td>
            <td style="width: 3%;">&nbsp;</td>
            <td style="width: 28.6%">&nbsp;</td>
            <td style="width: 28.7%">&nbsp;</td>
            <td style="width: 28.6%">&nbsp;</td>
        </tr>
        <tr>
            <th>9h-10h</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#introduction-to-quantum-programming-with-ket">
                    Introduction to quantum programming with Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#assessing-quantum-and-classical-approaches-to-combinatorial-optimization-testing-quadratic-speed-ups-for-heuristic-algorithms">
                    Assessing Quantum and Classical Approaches to Combinatorial Optimization: Testing Quadratic Speed-ups for Heuristic Algorithms (Pedro Contino)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#exploring-physics-problems-with-machine-learning-and-quantum-variational-algorithms">
                    Exploring Physics Problems with Machine Learning and Quantum Variational Algorithms (Felipe Fanchini)
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>10h-10h30</th>
            <td class="wspace">&nbsp;</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
        </tr>
        <tr>
            <th>10h30-11h30</th>
            <td class="wspace">&nbsp;</td>
            <td rowspan="2">
                <a class="schedule-a" href="#introduction-to-quantum-programming-with-ket">
                    Introduction to quantum programming with Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#emerging-technologies-in-quantum-computing-with-neutral-atoms">
                    Emerging Technologies in Quantum Computing with Neutral Atoms (Manuel Alejandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td rowspan="2">
                <a class="schedule-a" href="#posters-exhibition">
                    Pôsters Exhibition
                </a>
                /
                <a class="schedule-a" href="#vr-quantum-experience">
                    VR Quantum Experience
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>11h30-12h</th>
            <td class="wspace">&nbsp;</td>
            <!-- <td></td> -->
            <td>
                <a class="schedule-a" href="#vr-quantum-experience">
                    VR Quantum Experience (DEMO)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <!-- <td></td> -->
        </tr>
        <tr class="table-space">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>14h-15h30</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#introduction-to-quantum-programming-with-ket">
                    Introduction to quantum programming with Ket (Evandro)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#quantum-machine-learning">
                    Quantum Machine Learning (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/jNtEqG7oarvuY8EW7">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    INE106
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#quantum-machine-learning">
                    Quantum Machine Learning (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/EFxfVMDTr4oC625n8">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    LIICT6
                </a>
            </td>
        </tr>
        <tr>
            <th>15h-15h30</th>
            <td class="wspace">&nbsp;</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
            <td>* Coffee-Break *</td>
        </tr>
        <tr>
            <th>16h-17h</th>
            <td class="wspace">&nbsp;</td>
            <td>
                <a class="schedule-a" href="#learning-in-the-quantum-universe">
                    Learning in the Quantum Universe (Robert Huang)
                </a>
                <a class="location" href="https://maps.app.goo.gl/eBb7iEMGapFXZ7HP6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <td>
                <a class="schedule-a" href="#quantum-machine-learning">
                    Quantum Machine Learning (Lucas Friedrich & Tiago Farias)
                </a>
                <a class="location" href="https://maps.app.goo.gl/jNtEqG7oarvuY8EW7">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    INE106
                </a>
            </td>
            <td rowspan="2">
                <a class="schedule-a" href="#lightning-talks">
                    Lightning Talks
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    ARQ
                </a>
            </td>
        </tr>
        <tr>
            <th>17h-18h</th>
            <td class="wspace">&nbsp;</td>
            <td>
            </td>
            <td>
                <a class="schedule-a" href="#quantum-podcast">
                    Quantum Podcast
                </a>
                <a class="location" href="https://maps.app.goo.gl/7ZkNdjyVRZsx2Veh6">
                    <i class="fa fa-location-dot", style="margin-right: 5px;"></i>
                    EPS
                </a>
            </td>
            <!-- <td></td> -->
        </tr>
    </table>
</div>

<style>
    div.schedule-container {
        width: 100%;
        font-family: "Montserrat";
        font-size: 1.5rem;
        margin-top: 50px;
    }

    div.schedule-container a.schedule-a {
        text-decoration: none;
        color: #E5D6FF;
        font-family: "Montserrat";
        font-weight: 400;
    }
    div.schedule-container a.schedule-a:hover {
        color: white;
    }

    div.schedule-container table td,
    div.schedule-container table th {
        text-align: center;
        border: 2px solid #A56EFF;
    }

    div.schedule-container table td {
        position: relative;
        color: #E5D6FF;
        line-height: 2rem;
        padding: 25px 10px;
    }

    div.schedule-container table th {
        background-color: #3F235F;
    }

    div.schedule-container tr.table-space td,
    div.schedule-container tr.table-space th {
        border: none;
        line-height: 25px;
        height: 25px;
        padding: 0;
    }

    div.schedule-container td.wspace {
        border: none;
    }

    div.schedule-container tr:nth-child(n+3) th {
        line-height: 6rem;
    }

    div.schedule-container a.location {
        text-decoration: none;
        font-size: 1.2rem;
        opacity: 0.4;
        cursor: pointer;
        position: absolute;
        right: 0;
        bottom: 0;
        padding: 5px 5px;
    }

</style>

The activities will take place at the following locations:

- [João Ernesto Escosteguy Castro Auditorium](https://maps.app.goo.gl/bye1MVZTx8WnAJiUA) ([EPS](https://maps.app.goo.gl/Fu4kG4S2bP7Vemkq5) Auditorium)
- [LIICT6 Laboratory](https://maps.app.goo.gl/cE8pS84qQbsF1u3N9) non the 3rd floor of [CTC Block B](https://maps.app.goo.gl/9oEVkChJnQENDGgu6)
- INE106 Laboratory, located on the ground floor of the [INE Building](https://maps.app.goo.gl/9dQLzi9Mqf8GCgFZ6)
- Auditorium of the [Department of Architecture and Urbanism](https://maps.app.goo.gl/1ZfqMN7JU88SkNXA9)
