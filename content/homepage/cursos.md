---
title: 'Minicursos'
weight: 40
header_menu: true
template: 'odd'
---

------------

## Introdução à programação quântica com Ket 
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

Podemos usar superposição e emaranhamento para desenvolver aplicações aceleradas por computadores quânticos para resolver alguns problemas mais rápido do que qualquer supercomputador jamais poderia. Embora computadores quânticos capazes de superar computadores clássicos na resolução de problemas do mundo real ainda não sejam uma realidade, esperamos que eles estejam prontos em breve. Até lá, já podemos nos preparar para esse futuro, desenvolvendo e testando soluções aceleradas pela computação quântica hoje. Neste minicurso apresentaremos os principais conceitos da computação quântica aplicadas na linguagem de programação quântica Ket. Esperamos que todos os participantes interajam durante o curso, manifestando suas dúvidas e testando o que aprenderam.

[*Material*](https://bit.ly/3YlgmGK)

*Gravações*: [Parte 1](https://www.youtube.com/live/y9LqjEBkKz8?si=c2u3Su9w19tivBQJ), [Parte 2](https://www.youtube.com/live/YkQMp0qjiO0?si=ZAjy-ulGSDasHEMk), [Parte 3](https://www.youtube.com/live/HmoIHLuHnLE?si=SSml06kpajVj2ZRF).

------------

## Aprendizado de máquina quântica
[*Lucas Friedrich*](#lucas-friedrich) & [*Tiago Farias*](#tiago-de-souza-farias)

Neste minicurso, exploraremos uma variedade de algoritmos de aprendizado de máquina quântico, desde seus princípios até aplicações práticas. Iniciaremos com uma discussão sobre portas lógicas parametrizadas, funções custo, medidas, estimativas de gradiente e processos de otimização, que são fundamentos essenciais para compreender o funcionamento desses algoritmos. Prosseguiremos com uma análise da família de algoritmos de variação quântica, como o Variational Quantum Algorithm (VQA) e redes neurais quânticas. Discutiremos como esses algoritmos podem ser aplicados a problemas de classificação e regressão. Através destas implementações, os participantes poderão entender melhor como o aprendizado de máquina quântico pode ser utilizado para resolver problemas práticos de relevância científica.

[*Material 30/10*](https://github.com/tiago939/minicurso_qml)

[*Material 31/10*](https://bit.ly/40olgFq)

*Gravações*: [Parte 1](https://www.youtube.com/live/c1sq2TROLUE?si=sXcRdB5DLyCaeX41), [Parte 2](https://www.youtube.com/live/juLD395HCUw?si=HEBON3m4OqIdOZvP), [Parte 3](https://www.youtube.com/live/7vfy5FJyY4o?si=Ncn7uXe3zYDqdxec).
