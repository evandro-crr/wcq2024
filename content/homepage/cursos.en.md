---
title: 'Minicourses'
weight: 40
header_menu: true
template: 'odd'
---

------------

## Introduction to quantum programming with Ket
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

We can use superposition and entanglement to develop applications accelerated by quantum computers to solve some problems faster than any supercomputer ever could. Although quantum computers capable of outperforming classical computers in solving real-world problems are not yet a reality, we hope they will be ready soon. Until then, we can prepare for that future by developing and testing quantum computing-accelerated solutions today. In this mini-course we will present the main concepts of quantum computing applied in the quantum programming language Ket. We expect all participants to interact during the course, expressing their doubts and testing what they have learned.

[*Material*](https://bit.ly/3YlgmGK)

*Recordings*: [Part 1](https://www.youtube.com/live/y9LqjEBkKz8?si=c2u3Su9w19tivBQJ), [Part 2](https://www.youtube.com/live/YkQMp0qjiO0?si=ZAjy-ulGSDasHEMk), [Part 3](https://www.youtube.com/live/HmoIHLuHnLE?si=SSml06kpajVj2ZRF).

------------

## Quantum Machine Learning
[*Lucas Friedrich*](#lucas-friedrich) & [*Tiago Farias*](#tiago-de-souza-farias)

In this mini-course, we will explore a variety of quantum machine learning algorithms, from their principles to practical applications. We will begin with a discussion on parameterized quantum gates, cost functions, measurements, gradient estimations, and optimization processes, which are essential foundations for understanding the functioning of these algorithms. We will then proceed with an analysis of the family of variational quantum algorithms, such as the Variational Quantum Algorithm (VQA) and quantum neural networks. We will discuss how these algorithms can be applied to classification and regression problems. Through these implementations, participants will gain a better understanding of how quantum machine learning can be used to solve practical problems of scientific relevance.

[*Material 30/10*](https://github.com/tiago939/minicurso_qml)

[*Material 31/10*](https://bit.ly/40olgFq)

*Recordings*: [Part 1](https://www.youtube.com/live/c1sq2TROLUE?si=sXcRdB5DLyCaeX41), [Part 2](https://www.youtube.com/live/juLD395HCUw?si=HEBON3m4OqIdOZvP), [Part 3](https://www.youtube.com/live/7vfy5FJyY4o?si=Ncn7uXe3zYDqdxec).