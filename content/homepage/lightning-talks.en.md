---
title: 'Lightning Talks'
weight: 45
header_menu: true
---

**Dive into the Fast-Paced Lightning Talks!**

The Lightning Talks are a series of quick presentations focused on current topics in quantum computing. Each speaker has 15 minutes to share their ideas and findings, followed by 5 minutes for questions and discussions. This format aims to facilitate a rapid and direct exchange of knowledge.

[*See recording*](https://www.youtube.com/live/r8_LeluC96Y?si=iInFotcLPwsVPJdE)

<br>

### Schedule of Lightning Talks

1. **Packing Problem - A Quantum and Classical Approach**<br>
   *César Freitas (UFSC)*

2. **Benchmarking Complexity in Quantum Processors with a Majorization-Based Indicator**<br>
   *Nina M. O'Neill (CBPF)*

3. **Global Entanglement Estimation via Classical Shadows**<br>
   *João Pedro Engster (UFSC)*

4. **Shadow Measurements for Feedback-Based Quantum Optimizations**<br>
   *Leticia Bertuzzi (UFSC)*

5. **Optimizing Time in Quantum Circuit Execution**<br>
   *Eduardo Willwock Lussi (UFSC)*