---
title: 'Como Participar'
header_menu: false
template: 'light-grey'
weight: 13
---

Participar do evento é simples! Tudo o que você precisa fazer é se [inscrever](#inscrição) para garantir seu lugar. Uma vez inscrito, você terá duas opções para acompanhar as palestras:

1. **Ao vivo pelo YouTube:** Acompanhe as palestras de onde quiseres, através de transmissões ao vivo pelo nosso canal oficial no YouTube. Você terá a oportunidade de interagir por meio de comentários e absorver o conhecimento diretamente dos especialistas no campo da Computação Quântica.

2. **Presencialmente:** Se preferir uma experiência mais imersiva, convidamos você a se juntar a nós pessoalmente no local do evento. Confira a programação para obter detalhes sobre os horários e locais das palestras. Estamos ansiosos para recebê-lo em um ambiente inspirador, onde você poderá se conectar diretamente com outros entusiastas da área e mergulhar nas discussões revolucionárias.

Lembre-se de verificar a programação completa para obter informações sobre os horários, locais e palestrantes. Esta é a sua oportunidade de se envolver com a vanguarda da Computação Quântica e ampliar seus horizontes nesse campo em constante evolução. Mal podemos esperar para compartilhar essa jornada com você!