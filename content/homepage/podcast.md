---
title: 'Podcast Quântico'
weight: 48
header_menu: true
---

Em nosso Podcast Quântico, profissionais da computação quântica compartilharão suas experiências e perspectivas sobre a computação quântica. Junte-se a nós em conversas autênticas enquanto discutimos trajetórias profissionais, desafios enfrentados e os momentos que definem o caminho para o sucesso na indústria quântica. Se você está curioso sobre as possibilidades do mundo quântico ou está considerando suas próprias opções de carreira, este podcast oferece insights valiosos diretamente de quem está trilhando esse percurso!

Participantes:

- [*Rômulo Pinho*](#rômulo-pinho)

- [*Waldemir Cambiucci*](#waldemir-cambiucci)

[*Ver gravação*](https://www.youtube.com/live/_hFhIh7TCKc?si=M1b25hN7hXa4V4OB)