---
title: 'Talks'
weight: 30
header_menu: true
---

-------------------------------

## Exploring Physics Problems with Machine Learning and Quantum Variational Algorithms

[*Felipe Fanchini*](#felipe-fanchini)

Machine learning algorithms and their quantum variational counterparts have proven to be powerful tools for studying complex physical systems. In this talk, we will discuss how these approaches can be applied to fundamental problems in physics. We will present a series of studies we have developed, including the analysis of magnetic systems, non-Markovian processes, quantum synchronization, particle physics, quantum thermometry, and Hamiltonian tomography. These studies demonstrate the potential of these algorithms for exploring properties and behaviors in physical systems.

[*See recording*](https://www.youtube.com/live/fbkkk9ujuPw?si=RuvBR9iW8-TvjrUJ)

-------------------------------

## Quantum Core Technology Based on Neutral Atoms

[*Manuel A. Lefrán Torres*](#manuel-a-lefrán-torres)

In this seminar, we will explore the fundamental components of a quantum processor that utilizes neutral atoms. We begin by analyzing systems that enable the cooling and trapping of alkali neutral atom samples. Next, we will discuss Rydberg atomic states, which are crucial for creating a controlled environment, allowing both quantum simulation and computation. Finally, we will present an example that demonstrates how to develop a controlled quantum gate using precise sequences of light pulses (state rotation). Understanding these elements is vital for advancing quantum technologies based on neutral atoms, promising exciting innovations in quantum computing and simulation.

[*See recording*](https://www.youtube.com/live/iy2iwgB2kZQ?si=XZQ_clKqUom-77iW)

-------------------------------

## Assessing Quantum and Classical Approaches to Combinatorial Optimization: Testing Quadratic Speed-ups for Heuristic Algorithms

[*Pedro C. da Silva Costa*](#pedro-c-da-silva-costa)

We propose a benchmarking strategy to compare quantum and classical algorithms for combinatorial optimization, using the Sherrington-Kirkpatrick Hamiltonian as the benchmark problem. Classical and quantum structure search algorithms demonstrate more than quadratic speedup over brute-force methods. However, while there is hope that the quantum heuristic algorithm tested could provide a quadratic speedup over its classical counterpart, it exhibits worse asymptotic scaling in practice. These results highlight the current limitations of quantum heuristics and the strengths of structured search approaches. Our framework offers key insights for future research on benchmarking quantum heuristics, particularly in contexts where access to fault-tolerant quantum computers is unavailable.

[*See recording*](https://www.youtube.com/live/7Zg9L4nd6To?si=OslGqwmjYp4Cwr5g)

-------------------------------

# Learning in the Quantum Universe

[*Robert Huang*](#hsin-yuan-robert-huang)

I will present recent progress in building a rigorous theory to understand how scientists, machines, and future quantum computers could learn models of our quantum universe. The talk will begin with an experimentally feasible procedure for converting a quantum many-body system into a succinct classical description of the system, its classical shadow. Classical shadows can be applied to efficiently predict many properties of interest, including expectation values of local observables and few-body correlation functions. I will then build on the classical shadow formalism to answer two fundamental questions at the intersection of machine learning and quantum physics: Can classical machines learn to solve challenging problems in quantum physics? And can quantum machines learn exponentially faster and predict more accurately than classical machines? I will answer both questions positively through mathematical analysis and experimental demonstrations.

[*See recording*](https://www.youtube.com/live/cXR0hqB8j9U?si=V2AXkK-YqJ5bZip0)