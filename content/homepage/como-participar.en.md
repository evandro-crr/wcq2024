---
title: 'How to Participate'
header_menu: false
template: 'light-grey'
weight: 13
---

Participating in the event is simple! All you need to do is [sign up](#inscrição) to secure your spot. Once registered, you'll have two options to follow the lectures:

1. **Live on YouTube:** Tune in to the exciting talks from wherever you are, through live broadcasts on our official YouTube channel. You'll have the opportunity to interact through comments and directly absorb knowledge from experts in the field of Quantum Computing.

2. **In Person:** If you prefer a more immersive experience, we invite you to join us in person at the event venue. Check the schedule for details on the timings and locations of the lectures. We look forward to welcoming you to an inspiring environment, where you can directly connect with fellow enthusiasts and delve into revolutionary discussions.

Remember to check the complete schedule for information about timings, locations, and speakers. This is your opportunity to engage with the forefront of Quantum Computing and expand your horizons in this ever-evolving field. We can't wait to share this journey with you!