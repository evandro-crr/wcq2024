---
title: 'Registration'
weight: 15
header_menu: true
template: 'grey'
---

Stay tuned to our [Instagram](https://www.instagram.com/gcq_ufsc/) and [Youtube](https://www.youtube.com/@GCQUFSC) and don't miss the opportunity to sign up and participate in this transformative event. Get ready to join us on this journey of discoveries and advancements in Quantum Computing. Your presence is crucial to further enrich this unique experience. We eagerly await your registration!

<br>

<center>
    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdEBOLBLLV4t-hHEti-y9_TcPmDuN1vTQz6KfnIL-5ZQKNJog/viewform?embedded=true" class="content-btn">
    Subscribe
    </a>
</center>