---
title: 'Posters Exhibition'
weight: 47
header_menu: true
template: 'grey'
---

The Poster Exhibition provides a valuable opportunity for participants to share their research and discoveries with the academic and professional community. The aim is to create an interactive environment where participants can discuss their projects, findings, and ideas related to quantum computing. It's an excellent networking opportunity!

To present your poster, simply complete the registration form and bring your poster on the day of the event:

<br>
<center>
    <a href="https://forms.gle/qQgSjsnoZUnM9EuV7" class="content-btn">
        Submit your poster
    </a>
</center>