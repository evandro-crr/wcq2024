---
title: 'Exposição de Pôsteres'
weight: 47
header_menu: true
template: 'grey'
---

A Exposição de Pôsteres é uma oportunidade valiosa para os participantes compartilharem suas pesquisas e descobertas com a comunidade acadêmica e profissional. A ideia é tornar o evento um ambiente interativo onde os participantes possam discutir seus projetos, resultados e ideias relacionados à computação quântica. É uma excelente oportunidade para networking!

Para apresentar seu pôster, basta preencher o formulário de inscrição e trazer o seu pôster no dia do evento:

<br>
<center>
    <a href="https://forms.gle/qQgSjsnoZUnM9EuV7" class="content-btn">
        Submeter Poster
    </a>
</center>
