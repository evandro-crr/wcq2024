---
title: 'Palestras'
weight: 30
header_menu: true
---

-------------------------------

## Explorando Problemas da Física com Aprendizado de Máquina e Algoritmos Variacionais Quânticos

[*Felipe Fanchini*](#felipe-fanchini)

Os algoritmos de aprendizado de máquina e suas versões variacionais quânticas têm se mostrado ferramentas poderosas para o estudo de sistemas físicos complexos. Nesta palestra, discutiremos como essas abordagens podem ser aplicadas a problemas fundamentais na física. Iremos apresentar uma série de estudos que desenvolvemos, incluindo a análise de sistemas magnéticos, processos não-Markovianos, a sincronização quântica, física de partículas, termometria quântica e tomografia de Hamiltoniano. Esses estudos demonstram o potencial desses algoritmos para o estudo de propriedades e comportamentos em sistemas físicos.

[*Ver gravação*](https://www.youtube.com/live/fbkkk9ujuPw?si=RuvBR9iW8-TvjrUJ)

-------------------------------

## Tecnologias Emergentes em Computação Quântica com Átomos Neutros

[*Manuel A. Lefrán Torres*](#manuel-a-lefrán-torres)

Neste seminário, abordaremos os componentes fundamentais de um processador quântico que utiliza átomos neutros. Começamos analisando sistemas que facilitam o resfriamento e o aprisionamento de amostras de átomos neutros alcalinos. Em seguida, discutimos os estados atômicos de Rydberg, que são essenciais para criar um ambiente controlado, permitindo tanto a simulação quanto a computação quântica. Por último, apresentamos um exemplo que ilustra como desenvolver uma porta quântica controlada através de sequências precisas de pulsos de luz (rotação de estado). Compreender esses elementos é vital para o avanço das tecnologias quânticas baseadas em átomos neutros, trazendo promissoras inovações na computação e simulação quântica.

[*Ver gravação*](https://www.youtube.com/live/iy2iwgB2kZQ?si=XZQ_clKqUom-77iW)

-------------------------------

## Analisando Abordagens Clássicas e Quânticas para Otimização Combinatória: Testando Speedups Quadráticos em Algoritmos Heurísticos

[*Pedro C. da Silva Costa*](#pedro-c-da-silva-costa)

Propomos uma estratégia de benchmarking para comparar algoritmos quânticos e clássicos aplicados à otimização combinatória, utilizando o Hamiltoniano de Sherrington-Kirkpatrick como problema de referência. Algoritmos de busca estruturada, tanto clássicos quanto quânticos, demonstram speedup superior ao quadrático em relação aos métodos de força bruta. No entanto, embora haja expectativa de que o algoritmo heurístico quântico testado possa fornecer um speedup quadrático em relação ao seu equivalente clássico, ele apresenta escalabilidade assintótica pior na prática. Esses resultados destacam as limitações atuais dos algoritmos heurísticos quânticos e as vantagens das abordagens de busca estruturada. Nossa estrutura oferece insights importantes para pesquisas futuras sobre benchmarking de heurísticas quânticas, especialmente em contextos onde o acesso a computadores quânticos tolerantes a falhas não está disponível.

[*Ver gravação*](https://www.youtube.com/live/7Zg9L4nd6To?si=OslGqwmjYp4Cwr5g)

-------------------------------

# Aprendizagem no Universo Quântico

[*Robert Huang*](#hsin-yuan-robert-huang)

Nesta palestra, explorarei os avanços recentes na construção de uma rigorosa teoria sobre como cientistas, máquinas e futuros computadores quânticos podem aprender modelos que descrevem o universo quântico. Iniciaremos com um processo viável de conversão de um sistema quântico de muitos corpos em uma descrição clássica compacta, conhecida como classical shadow. Essa técnica permite prever com eficiência diversas propriedades, como valores esperados de observáveis locais e funções de correlação entre poucos corpos. A partir desse formalismo, abordaremos duas questões fundamentais na interseção entre aprendizado de máquina e física quântica: Máquinas clássicas podem aprender a resolver problemas complexos da física quântica? E máquinas quânticas podem aprender exponencialmente mais rápido e realizar previsões mais precisas do que suas contrapartes clássicas? Ambas as questões serão respondidas de forma afirmativa, por meio de uma combinação de rigorosas análises matemáticas e demonstrações experimentais.

[*Ver gravação*](https://www.youtube.com/live/cXR0hqB8j9U?si=V2AXkK-YqJ5bZip0)