---
title: 'Edições Anteriores'
header_menu: false
weight: 17
---

À medida que nos preparamos para a emocionante sétima edição do Workshop de Computação Quântica, vale a pena relembrar as edições anteriores que nos trouxeram até aqui:

- [VI Workshop de Computação Quântica (2023)](https://workshop-cq.ufsc.br/2023/)
- [V Workshop de Computação Quântica (2022)](https://workshop-cq.ufsc.br/2022/)
- [IV Workshop de Computação Quântica (2021)](https://workshop-cq.ufsc.br/2021/)
- [III Workshop de Computação Quântica (2020)](https://workshop-cq.ufsc.br/2020/)
- [II Workshop de Computação Quântica (2019)](https://workshop-cq.ufsc.br/2019/)
- [I Workshop de Computação Quântica (2018)](https://workshop-cq.ufsc.br/2018/)