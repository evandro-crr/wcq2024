---
title: 'Sobre'
weight: 10
---

Com grande entusiasmo, anunciamos a mais recente edição do Workshop de Computação Quântica. Este evento reúne renomados especialistas da academia e indústria, com o propósito de manter a comunidade brasileira atualizada sobre os rápidos avanços no campo da Computação Quântica. Esta revolução tem como base a aplicação da mecânica quântica para a criação de uma nova lógica de processamento de informações.

Apesar de estar em seus estágios iniciais, os processadores quânticos [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502) e [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), todos provenientes da China, juntamente com o notável processador fotônico [Borealis](https://www.nature.com/articles/s41586-022-04725-x) desenvolvido pela conceituada empresa canadense Xanadu, já ultrapassaram a capacidade de processamento de supercomputadores de renome, como o Summit e o Sunway TaihuLight. Em alguns casos, os computadores quânticos demonstraram ser até 10 milhões de bilhões de vezes mais velozes do que suas contrapartes clássicas em simulações exatas!

O [Grupo de Computação Quântica da UFSC](http://www.gcq.ufsc.br/) tem a honra de convidá-lo a se juntar a nós nessa empolgante revolução científica e tecnológica. Durante o evento, ofereceremos palestras e minicursos para apresentar as mais recentes conquistas nesse campo dinâmico. Abaixo, detalhamos mais informações sobre o evento e como você pode participar ativamente dessa jornada rumo ao futuro da computação.
