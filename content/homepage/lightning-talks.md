---
title: 'Lightning Talks'
weight: 45
header_menu: true
---

**Desbrave em ritmo acelerado com as Lightning Talks!**

As Lightning Talks são uma série de apresentações rápidas voltadas para temas atuais em computação quântica. Cada palestrante tem 15 minutos para compartilhar suas ideias e descobertas, seguidos por 5 minutos de perguntas e discussões. O formato busca facilitar uma troca rápida e direta de conhecimento.

[*Ver gravação*](https://www.youtube.com/live/r8_LeluC96Y?si=iInFotcLPwsVPJdE)

<br>

### Cronograma das Lightning Talks

1. **Problema do Empacotamento - Uma Abordagem Quântica e Clássica**<br>
   *César Freitas (UFSC)*

2. **Benchmarking da Complexidade em Processadores Quânticos com um Indicador Baseado em Majorização**<br>
   *Nina M. O'Neill (CBPF)*

3. **Caracterização Global de Emaranhamento via Classical Shadows**<br>
   *João Pedro Engster (UFSC)*

4. **Medidas de Classical Shadows para Otimizações Quânticas Baseadas em Feedback**<br>
   *Leticia Bertuzzi (UFSC)*

5. **Otimizando Tempo na Execução de Circuitos Quânticos**<br>
   *Eduardo Willwock Lussi (UFSC)*