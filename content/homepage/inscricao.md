---
title: 'Inscrição'
weight: 15
header_menu: true
template: 'grey'
---

Acompanhe nosso [Instagram](https://www.instagram.com/gcq_ufsc/) e [Youtube](https://www.youtube.com/@GCQUFSC) não perca a oportunidade de se inscrever e participar desse evento transformador. Esteja preparado para se juntar a nós nessa jornada de descobertas e avanços na Computação Quântica. Sua presença é fundamental para enriquecer ainda mais essa experiência única. Aguardamos ansiosamente por sua inscrição!

<br>

<center>
    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdEBOLBLLV4t-hHEti-y9_TcPmDuN1vTQz6KfnIL-5ZQKNJog/viewform?embedded=true" class="content-btn">
    Inscreva-se
    </a>
</center>