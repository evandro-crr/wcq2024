---
title: 'Hackathon'
weight: 46
header_menu: true
template: 'light-grey'
---

O Hackathon de Computação Quântica é uma oportunidade única para explorar desafios práticos e aplicar seus conhecimentos na área. Os desafios serão anunciados ao longo dos minicursos do evento, e os participantes terão até o **04/11 - 12h** para enviar suas soluções.  

Embora o hackathon não tenha premiação, ele oferece um ambiente colaborativo e de aprendizado, incentivando a troca de ideias e o desenvolvimento de habilidades em computação quântica.  

Fique atento! Mais informações serão divulgadas em breve.

- [Desafio: Classical Shadows e Ket [pdf]](Hackathon.pdf)
- [Desafio: Aprendizado de Máquina Quântico [pdf]](Hackathon_UFSC_2024___QML.pdf)

<center>
    <a href="https://forms.gle/tNwyoFSdVSHELiav7" class="content-btn">
    Submeter Soluções 
    </a>
</center>