---
title: 'Palestrantes'
weight: 50
header_menu: true
template: 'odd'
---

--------------------------------------

## Evandro C. R. da Rosa
*Quantuloop (Brasil)*

![Evandro C. R. da Rosa](fotos/evandro.jpg)

Evandro é cofundador da startup de computação quântica Quantuloop, doutorando e mestre em Ciência da Computação pela Universidade Federal de Santa Catarina (UFSC). Sua área de pesquisa abrange programação e simulação de computadores quânticos. Ele é o idealizador da plataforma de programação quântica de código aberto Ket, liderando seu desenvolvimento no âmbito do Grupo de Computação Quântica da UFSC.

--------------------------------------

## Felipe Fanchini
*Universidade Estadual Paulista (Brasil)*

![Felipe Fanchini](fotos/felipe.jpg)

Felipe é professor e pesquisador da Universidade Estadual Paulista (Unesp) com experiência em informação e computação quântica, bem como em aprendizado de máquina. É membro da coordenação do programa FAPESP em tecnologias quânticas. Suas pesquisas estão relacionadas a sistemas quânticos abertos, estratégias de proteção da informação quântica e análise de sistemas quânticos utilizando técnicas de aprendizado de máquina. Em anos recentes, tem se dedicado ao aprendizado de máquina quântico e otimização quântica realçando a confluência de suas principais áreas de expertise.

--------------------------------------

## Hsin-Yuan (Robert) Huang
*Caltech (Estados Unidos)*

![Hsin-Yuan (Robert) Huang](fotos/robert.jpg)

Hsin-Yuan Huang é cientista visitante no MIT e cientista de pesquisa no Google Quantum AI. Concluiu seu doutorado em 2023 sob a orientação de John Preskill e Thomas Vidick. Também conhecido como Robert, ele começará como Professor Assistente de Física Teórica no Caltech em 2025. Sua pesquisa foca na teoria da informação quântica, física quântica de muitos corpos e teoria da aprendizagem para avançar nossa compreensão dos fenômenos quânticos e acelerar descobertas científicas. Seu objetivo é desenvolver máquinas quânticas capazes de explorar novas fronteiras na mecânica quântica e além.

--------------------------------------

## Lucas Friedrich
*Universidade Federal de Santa Maria (Brasil)*

![Lucas Friedrich](fotos/lucas.jpg)

Lucas Friedrich é bacharel em Física pela Universidade Federal de Santa Maria, onde atualmente realiza seu doutorado em Física. Sua pesquisa está focada no emergente campo do aprendizado de máquina quântico, explorando como os computadores quânticos podem oferecer vantagens significativas na criação de modelos mais robustos e eficientes em comparação com os métodos clássicos. Além disso, ele está envolvido no desenvolvimento de novos métodos de otimização e na aplicação prática dos algoritmos quânticos variacionais.

--------------------------------------

## Manuel A. Lefrán Torres
*Universidade de São Paulo (Brasil)*

![Manuel A. Lefrán Torres](fotos/manuel.jpg)

Manuel A. Lefrán Torres possui bacharelado em Física pela Universidade Central Marta Abreu de Las Villas (2014), mestrado em Matemática Aplicada pela mesma instituição (2017) e doutorado em Física pela Universidade de São Paulo (2024). Atualmente, realiza pós-doutorado na Universidade de São Paulo, trabalhando no projeto "Feixe Molecular em um Único Estado Quântico". Possui experiência na área de Física, com ênfase em Física Atômica e Molecular, e tem interesse em pesquisas relacionadas a átomos e moléculas frias, sensores quânticos e computação quântica.

--------------------------------------

## Pedro C. da Silva Costa
*Universidade de Tecnologia de Sydney (Austrália)*

![Pedro C. da Silva Costa](fotos/pedro.jpg)

Pedro C. S. Costa é pesquisador assistente na Universidade de Tecnologia de Sydney (UTS) e pesquisador honorário da Universidade Macquarie. Além disso, presta consultoria para duas startups americanas, sendo uma delas a BosonQ Psi. Concluiu seu mestrado em Física Teórica na Unesp e seu doutorado no Centro Brasileiro de Pesquisas Físicas sob a orientação de Fernando de Melo. Durante o seu doutorado, iniciou a sua especialização em algoritmos quânticos, tendo como seu primeiro resultado o algorítimo quântico para a simulação da equação de onda, desenvolvido com Stephen Jordan durante seu período trabalhando como pesquisador visitante na Universidade de Maryland nos Estados Unidos. Fez seu pós-doutorado na Universidade Macquarie localizada em Sydney, Austrália (AUS), com o grupo do Prof. Dominic Berry. Em seguida, trabalhou por um ano e meio para o governo australiano com o intuito de aplicar soluções quânticas para a rede de transporte. Possui os algoritmos quânticos mais eficientes para equações diferenciais lineares e não-lineares e o algoritmo mais eficiente possível para resolver sistemas de equações lineares.

--------------------------------------

## Rômulo Pinho
*Dell (Brasil)*

![Rômulo Pinho](fotos/romulo.jpg)

Rômulo Pinho é pesquisador e Distinguished Engineer na Dell Technologies, atuando principalmente nas áreas de IA e Computação Quântica, e ajudando a empresa a investigar e definir sua atuação na interseção entre as duas áreas. Possui doutorado em Física (processamento de imagens médicas) pela Universidade de Antuérpia, na Bélgica, e soma 25 anos de experiência em P&D entre indústria e academia, em setores como TI, O&G, saúde e televisão. Rômulo ingressou na Dell Technologies (antiga EMC) em 2014 para trabalhar como pesquisador em IA. Desde então, liderou pesquisas em áreas como compressão de dados, otimização de workflows científicos e cargas de trabalho, detecção de padrões e anomalias, visão computacional, NLP e, mais recentemente, nas interseções entre computação quântica e IA generativa. Publicou mais de 20 artigos científicos em periódicos e conferências e já tem mais de 100 patentes depositadas.

--------------------------------------

## Tiago de Souza Farias
*Universidade Federal de São Carlos (Brasil)*

![Tiago de Souza Farias](fotos/tiago.jpg)

Tiago Farias é doutor em Física pela Universidade Federal de Santa Maria e atualmente realiza pós-doutorado na Universidade Federal de São Carlos, com foco em computação quântica e aprendizado de máquina. Com experiência em algoritmos clássicos e quânticos, seus interesses de pesquisa incluem computação quântica, física estatística e otimização. Tiago se dedica a explorar as fronteiras da tecnologia quântica aplicada ao aprendizado de máquina, buscando contribuir de forma significativa para a área.

--------------------------------------

## Waldemir Cambiucci
*Microsoft (Brasil)*

![Waldemir Cambiucci](fotos/waldemir.jpg)

Waldemir é diretor de inovação e tecnologias emergentes na Microsoft Brasil, suportando discussões sobre transformação digital e inovação com clientes corporativos. Tem atuado em projetos e discussões sobre IoT, Industrial IoT, Artificial Intelligence, Machine Learning, Responsible AI, Cloud Solutions e Quantum Solutions Architecture. Com mais de 25 anos de experiência em TI, tem participado dos principais fóruns de tecnologia no Brasil e no exterior. Waldemir é Engenheiro de Computação e Mestre em Engenharia Elétrica pela Escola Politécnica da Universidade de São Paulo (EPUSP). Atualmente é PhD candidato pela Universidade de São Paulo, com pesquisa em algoritmos quânticos e computação quântica distribuída. É membro do IEEE Quantum Community, Qiskit Developer Associate Certified e Microsoft Azure Quantum Ambassador no Brasil.
