---
title: 'Previous Editions'
header_menu: false
weight: 17
---

As we prepare for the exciting seventh edition of the Quantum Computing Workshop, it's worth reflecting on the previous editions that have brought us to this point:

- [VI Quantum Computing Workshop (2023)](https://workshop-cq.ufsc.br/2023/)
- [V Quantum Computing Workshop (2022)](https://workshop-cq.ufsc.br/2022/)
- [IV Quantum Computing Workshop (2021)](https://workshop-cq.ufsc.br/2021/)
- [III Quantum Computing Workshop (2020)](https://workshop-cq.ufsc.br/2020/)
- [II Quantum Computing Workshop (2019)](https://workshop-cq.ufsc.br/2019/)
- [I Quantum Computing Workshop (2018)](https://workshop-cq.ufsc.br/2018/)