---
title: 'VR Quantum Experience'
weight: 49
header_menu: true
template: 'light-grey'
---

The VR Quantum Experience is an immersive and interactive Virtual Reality experience designed to make the understanding of fundamental quantum mechanics concepts more accessible.

During the demonstration, participants will dive into a representation of Schrödinger's wave functions applied to the hydrogen atom. In a virtual environment with 6 degrees of freedom, they will be able to explore different configurations of atomic orbitals, move freely around the atom, scale it up or down, and rotate it.

This dynamic approach offers a unique and accessible perspective for visualizing complex quantum phenomena!

[*See recording*](https://www.youtube.com/live/yWPuctfjIao?si=-g47_hfwd1iFr0E5)