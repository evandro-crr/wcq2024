---
title: 'About'
weight: 10
---

With great enthusiasm, we announce the latest edition of the Quantum Computing Workshop. This event brings together renowned experts from academia and industry, with the purpose of keeping the Brazilian community updated on the rapid advancements in the field of Quantum Computing. This revolution is based on the application of quantum mechanics to create a new logic of information processing.

Despite being in its early stages, quantum processors [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502), and [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), all from China, along with the notable photonic processor [Borealis](https://www.nature.com/articles/s41586-022-04725-x) developed by the esteemed Canadian company Xanadu, have already surpassed the processing power of renowned supercomputers like Summit and Sunway TaihuLight. In some cases, quantum computers have demonstrated speeds up to 10 million billion times faster than their classical counterparts in exact simulations!

The [Quantum Computing Group of UFSC](http://www.gcq.ufsc.br/) is honored to invite you to join us in this exciting scientific and technological revolution. Throughout the event, we will offer lectures and workshops to present the latest achievements in this dynamic field. Below, we provide more information about the event and how you can actively participate in this journey towards the future of computing.