---
title: 'Organização'
weight: 70
template: 'light-grey'
---

* Prof. Eduardo Inacio Duzzioni, Dr. (UFSC) - Coordenador
* Profª. Jerusa Marchi, Drª. (UFSC) - Coordenadora
* Prof. Paulo Mafra, Dr. (UFSC)
* Prof. Pedro Castellucci, Dr. (UFSC)
* Evandro Chagas Ribeiro da Rosa, Me. (UFSC/Quantuloop)
* Eduardo Palmeira, Me. (UFSC)
* João Pedro Engster, Me. (UFSC)
* Otto Menegasso Pires, Me. (LAQCC/SENAI CIMATEC)
* Eduardo Willwock Lussi, B.Sc. (UFSC/Quantuloop)
* Gabriel Medeiros Lopes, B.Sc. (UFSC)
* Letícia Bertuzzi, B.Sc. (UFSC)
* César Freitas (UFSC)
* Ruan Luiz Molgero Lopes (UFSC)
* Vinícius Luz Oliveira (UFSC)
