---
title: 'Hackathon'
weight: 46
header_menu: true
template: 'light-grey'
---

The Quantum Computing Hackathon is a unique opportunity to tackle practical challenges and apply your knowledge in the field. The challenges will be announced during the event's minicourses, and participants will have until **11/04**, to submit their solutions.  

Although the hackathon does not offer prizes, it provides a collaborative and learning-focused environment, encouraging the exchange of ideas and the development of skills in quantum computing.  

Stay tuned! More information will be available soon.

- [Desafio: Classical Shadows e Ket [pdf]](../Hackathon.pdf)
- [Desafio: Aprendizado de Máquina Quântico [pdf]](../Hackathon_UFSC_2024___QML.pdf)

<center>
    <a href="https://forms.gle/tNwyoFSdVSHELiav7" class="content-btn">
    Submit Solutions 
    </a>
</center>