---
title: 'Quantum Podcast'
weight: 48
header_menu: true
---

In our Quantum Podcast, experts will share their experiences and perspectives in the field of quantum computing. Join us for authentic conversations as we delve into professional journeys, challenges faced, and the defining moments that shape the path to success in the quantum industry. Whether you're curious about the possibilities of the quantum world or contemplating your own career options, this podcast offers valuable insights directly from those treading this path!

Participants:

- [*Rômulo Pinho*](#rômulo-pinho)

- [*Waldemir Cambiucci*](#waldemir-cambiucci)

[*See recording*](https://www.youtube.com/live/_hFhIh7TCKc?si=M1b25hN7hXa4V4OB)